FROM php:7.4.7-fpm-alpine3.12

# BCMath - PHP Extension, not existed in this Image
# MySQLi - PHP Extension, Laravel:
# https://github.com/docker-library/php/issues/279
RUN docker-php-ext-install bcmath mysqli pdo pdo_mysql

# Install NPM
RUN apk add --update npm

# Install Composer
RUN apk add composer
# Speed up Composer
RUN composer global require hirak/prestissimo

